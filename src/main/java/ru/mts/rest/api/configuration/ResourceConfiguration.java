package ru.mts.rest.api.configuration;

import org.glassfish.jersey.server.ResourceConfig;
import ru.mts.rest.api.service.UserProfileService;

public class ResourceConfiguration extends ResourceConfig {
    public ResourceConfiguration() {
        packages("ru.mts.rest.api");
        register(UserProfileService.class);
    }

}