package ru.mts.rest.api.util;

import ru.mts.rest.api.model.ErrorMessageModel;

public class ErrorMessageModelFactory {
    public static ErrorMessageModel getLoginRequiredMsg() {
        return new ErrorMessageModel("1", "Login is required", "Msg description");
    }
    public static ErrorMessageModel profileAlreadyExistsMsg() {
        return new ErrorMessageModel("2", "User profile with login already exists", "Msg description");
    }
    public static ErrorMessageModel profileNotFoundMsg() {
        return new ErrorMessageModel("3", "User profile not found", "Msg description");
    }

}
