package ru.mts.rest.api.service;

import io.swagger.oas.annotations.Operation;
import io.swagger.oas.annotations.media.Content;
import io.swagger.oas.annotations.media.Schema;
import io.swagger.oas.annotations.responses.ApiResponse;
import org.apache.commons.lang3.StringUtils;
import ru.mts.data.FakeData;
import ru.mts.rest.api.model.ErrorMessageModel;
import ru.mts.rest.api.model.UserProfileModel;
import ru.mts.rest.api.util.ErrorMessageModelFactory;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.ArrayList;
import java.util.List;


@Path("/user-profiles")
@Produces(MediaType.APPLICATION_JSON)
public class UserProfileService {

    @GET
    @Path("{login}")
    @Operation(summary = "Find user profile by unique login",
            description = "Login must not be null",
            responses = {
                    @ApiResponse(
                            content = @Content(mediaType = "application/json",
                                    schema = @Schema(implementation = UserProfileModel.class)))
            })
    public Response getUserProfileByLogin(@PathParam("login") String login) {
        // Assuming we have not DB

        if (login == null || StringUtils.isEmpty((login))) {
            return Response.status(500).entity(ErrorMessageModelFactory.getLoginRequiredMsg()).build();
        }
        UserProfileModel model = FakeData.fakeUserProfiles.stream().filter(x -> login.equals(x.getLogin())).findAny().orElse(null);
        if (model == null) {
            return Response.status(500).entity(ErrorMessageModelFactory.profileNotFoundMsg()).build();
        }
        return Response.status(200).entity(model).build();
    }
    @GET
    public List<UserProfileModel> getUserProfiles() {
        List<UserProfileModel> models = new ArrayList<UserProfileModel>();
        models.addAll(FakeData.fakeUserProfiles);
        return models;
    }

    @POST
    @Path("/save")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response saveUserProfile(UserProfileModel model) {
        if (model == null || StringUtils.isEmpty(model.getLogin())) {
            return Response.status(500).entity(ErrorMessageModelFactory.getLoginRequiredMsg()).build();
        }
        String result = "User profile saved : " + model;
        UserProfileModel alreadyExistUserProfile = FakeData.fakeUserProfiles.stream().filter(x -> model.getLogin().equals(x.getLogin())).findAny().orElse(null);
        if (alreadyExistUserProfile != null) {
            return Response.status(500).entity(ErrorMessageModelFactory.profileAlreadyExistsMsg()).build();
        }
        FakeData.fakeUserProfiles.add(model);
        return Response.status(200).entity(result).build();

    }

    @DELETE
    @Path("/delete/{login}")
    public Response deleteUserProfile(@PathParam("login") String login) {
        UserProfileModel model = FakeData.fakeUserProfiles.stream().filter(x -> login.equals(x.getLogin())).findAny().orElse(null);
        int removeIndex = FakeData.fakeUserProfiles.indexOf(model);
        if(model==null) {

            return Response.status(404).entity(ErrorMessageModelFactory.profileNotFoundMsg()).build();
        }
        FakeData.fakeUserProfiles.remove(removeIndex);
        return Response.status(200).build();
    }

}
