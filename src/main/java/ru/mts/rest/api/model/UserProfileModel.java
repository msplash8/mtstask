package ru.mts.rest.api.model;


public class UserProfileModel {
    private String displayName;
    private String login;
    private String password;
    // DATE IN MILLISECONDS
    private Long birthDate;
    // 1 - FOR MALE; 2 - FOR FEMALE;
    private Integer gender;

    public UserProfileModel() {
    }

    public UserProfileModel(String displayName, String login, String password, Long birthDate, Integer gender) {
        this.displayName = displayName;
        this.login = login;
        this.password = password;
        this.birthDate = birthDate;
        this.gender = gender;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Long getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(Long birthDate) {
        this.birthDate = birthDate;
    }

    public Integer getGender() {
        return gender;
    }

    public void setGender(Integer gender) {
        this.gender = gender;
    }
}
