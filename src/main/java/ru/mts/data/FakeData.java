package ru.mts.data;

import ru.mts.rest.api.model.UserProfileModel;

import java.util.ArrayList;
import java.util.List;

public class FakeData {
    public static List<UserProfileModel> fakeUserProfiles = new ArrayList<UserProfileModel>();
    static {
        UserProfileModel fakeModel1 = new UserProfileModel(
                "Display Name", "login", "password", 1L, 1);
        UserProfileModel fakeModel2 = new UserProfileModel(
                "Display Name 2", "login2", "password 2", 123L, 2);
        fakeUserProfiles.add(fakeModel1);
        fakeUserProfiles.add(fakeModel2);
    }
}
